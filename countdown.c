///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Christian Yang <ccyang3@hawaii.edu>
// @date   06/02/21
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#ifdef _WIN32
   #include <Windows.h>
#else
   #include <unistd.h>
#endif

int main() {

   char buffer[80];
   struct tm start;
   time_t begintime, secondsbegin, secondsnow, secondsdiff, years, days, hours, mins, seconds;

   /*Initialize the reference time*/
   start.tm_year = 2014-1900;
   start.tm_mon = 1-1;
   start.tm_mday = 21;
   start.tm_hour = 4;
   start.tm_min = 26;
   start.tm_sec = 7;
   start.tm_isdst = -1;

   /*Convert reference time from struct to a time_t value*/
   begintime = mktime(&start);
   /*Print the reference time*/
   strftime(buffer, sizeof(buffer), "%c", &start);
   printf("Reference time: %s\n", buffer);

   while(1){
   #ifdef DEBUG
      printf("%ld\n", begintime);
   #endif
      secondsnow = time(NULL);
   #ifdef DEBUG
      printf("%ld\n", secondsnow); 
   #endif
      secondsdiff = secondsnow - begintime;
   
      years = secondsdiff/31536000;
      days = (secondsdiff%31536000)/86400;
      hours = ((secondsdiff%31536000)%86400)/3600;
      mins = (secondsdiff%3600)/60;
      seconds = (secondsdiff%3600)%60;
 
      printf("Years: %ld, days: %ld, hours: %ld, mins: %ld, seconds: %ld\n",years,days, hours, mins, seconds);
      
      sleep(1);
      }
   return 0;
}
